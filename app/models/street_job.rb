class StreetJob < ActiveRecord::Base
  enum operation_type: [ :bank, :visit_client ]
  enum status: [ :done, :canceled ]
  belongs_to :jobable, polymorphic: true
  after_save :clean_street_jobs_same_day, :send_email_notification

  validates :operation_type, presence: true

  protected
  def clean_street_jobs_same_day
    StreetJob.where(operation_type: self.operation_type, scheduled_at: self.scheduled_at.beginning_of_day..self.scheduled_at.end_of_day,
      status: nil, jobable_id: self.jobable_id).where("id <> ?", self.id).delete_all
  end

  def valid_to_send_email_notification?
    self.status.nil? && !self.send_pre_sale? && !self.receive_pre_sale?
  end

  def send_email_notification
    Notifier.delay.street_job(self) if self.valid_to_send_email_notification?
  end
end
