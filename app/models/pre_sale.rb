class PreSale < ActiveRecord::Base
  enum status: [ :with_us, :with_client, :converted_into_sale, :returned, :absent_client, :return_other_day ]
  belongs_to :client
  has_many :pre_sale_products, dependent: :destroy
  has_many :products, through: :pre_sale_products
  has_many :sales
  accepts_nested_attributes_for :pre_sale_products, allow_destroy: true, reject_if: proc { |attributes| attributes['product_name'].blank? || attributes['quantity'].blank? }
  delegate :name, to: :client, prefix: true, allow_nil: true
  attr_writer :client_name

  validates :client_name, :receive_pre_sale_at, :status, presence: true

  before_validation :set_client_by_name
  after_save :workflow_sale

  def workflow_sale
    if self.converted_into_sale?
      sale = Sale.new(client_id: self.client.id, pre_sale_id: self.id,
        sell_at: Time.now,
        payment_division: 1,
        payment_division_sell_price: self.pre_sale_products.to_a.sum{|t| t.price * t.quantity}
      )
      sale.save(validate: false)
      self.pre_sale_products.each do |pre_sale_product|
        sale.sale_products.build(product_id: pre_sale_product.product.id, price: pre_sale_product.price, quantity: pre_sale_product.quantity, detail: pre_sale_product.detail).save(validate: false)
      end
    end
  end

  def self.send_today
    PreSale.where(status: PreSale.statuses_for_send_today).where("send_pre_sale_at <= ?", Date.today.end_of_day)
  end

  def self.receive_today
    PreSale.where(status: PreSale.statuses_for_receive_today).where("receive_pre_sale_at <= ?", Date.today)
  end

  def self.statuses_for_send_today
    [ PreSale.statuses[:with_us] ]
  end

  def self.statuses_for_receive_today
    [ PreSale.statuses[:with_client], PreSale.statuses[:absent_client], PreSale.statuses[:return_other_day] ]
  end

  def save_and_schedule
    if save
      schedule_send_pre_sale
      schedule_receive_pre_sale
    end
  end

  def update_and_schedule(values)
    if update(values)
      schedule_send_pre_sale
      schedule_receive_pre_sale
    end
  end

  def self.send_pre_sale_emails_at_date(date = Date.today)
    PreSale.where(status: PreSale.statuses_for_send_today).where("send_pre_sale_at <= ?", Date.today.end_of_day).each do |pre_sale|
      Notifier.delay.send_pre_sale(pre_sale)
    end
  end

  def self.receive_pre_sale_emails_at_date(date = Date.today)
    PreSale.where(status: PreSale.statuses_for_receive_today).where("receive_pre_sale_at <= ?", Date.today.end_of_day).each do |pre_sale|
      Notifier.delay.receive_pre_sale(pre_sale)
    end
  end

  def name
    "#{self.client_name} - #{self.products.pluck(:name).to_sentence}"
  end

  protected

  def set_client_by_name
    self.client = Client.find_or_create_by(name: @client_name) unless @client_name.blank?
  end

  def schedule_receive_pre_sale
    if self.receive_pre_sale_at >= Date.today.beginning_of_day && self.receive_pre_sale_at <= Date.today.end_of_day
      Notifier.delay.receive_pre_sale(self)
    end
    true
  end

  def schedule_send_pre_sale
    return true if self.send_pre_sale_at.blank?
    if self.send_pre_sale_at >= Date.today.beginning_of_day && self.send_pre_sale_at <= Date.today.end_of_day
      Notifier.delay.send_pre_sale(self)
    end
    true
  end
end
