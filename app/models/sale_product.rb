class SaleProduct < ActiveRecord::Base
  belongs_to :product
  belongs_to :sale
  delegate :name, to: :product, prefix: true, allow_nil: true
  attr_writer :product_name
  after_validation :set_product_by_name
  protected

  def set_product_by_name
    self.product = Product.find_or_create_by(name: @product_name)
  end
end
