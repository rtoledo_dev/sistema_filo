class Sale < ActiveRecord::Base
  enum status: [ :finished_and_received, :finished_and_not_received, :returned ]
  enum payment_method: [ :creditcard, :money, :check, :online_transfer, :installment_sales ]
  has_many :daily_cashes
  has_many :sale_products, dependent: :destroy
  has_many :products, through: :sale_products
  accepts_nested_attributes_for :sale_products, allow_destroy: true, reject_if: proc { |attributes| attributes['product_name'].blank? || attributes['quantity'].blank? }
  belongs_to :sale
  belongs_to :pre_sale
  belongs_to :user
  belongs_to :client
  after_create :receive_payment

  delegate :name, to: :client, prefix: true, allow_nil: true
  attr_writer :client_name

  validates :client_name, :user_id, :status, :payment_division, :payment_division_sell_price, :payment_method, presence: true

  def receive_payment
    if self.finished_and_received? && self.money?
      self.daily_cashes.money.build(amount: self.amount_sold).save
    end
  end

  def self.to_receive
    Sale.where(status: Sale.statuses[:finished_and_not_received])
  end

  def amount
    self.sale_products.to_a.sum{|t| t.price * t.quantity}
  rescue
    0
  end

  def amount_sold
    (self.payment_division * self.payment_division_sell_price) + self.money_on_create.to_f
  rescue
    0
  end

  def offer_to_client
    amount - self.amount_sold
  end

  def name
    "#{self.client_name} (#{self.amount_sold}) - #{self.products.pluck(:name).to_sentence}"
  end
end
