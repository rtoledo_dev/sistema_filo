class DailyCash < ActiveRecord::Base
  enum receive_type: [ :money, :check ]
  belongs_to :sale
  validates :amount, :receive_type, presence: true
end
