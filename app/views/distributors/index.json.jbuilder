json.array!(@distributors) do |distributor|
  json.extract! distributor, :id, :name, :contact, :contact_phone
  json.url distributor_url(distributor, format: :json)
end
