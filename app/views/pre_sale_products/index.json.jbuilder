json.array!(@pre_sale_products) do |pre_sale_product|
  json.extract! pre_sale_product, :id, :pre_safe_id, :product_id, :price, :quantity, :detail
  json.url pre_sale_product_url(pre_sale_product, format: :json)
end
