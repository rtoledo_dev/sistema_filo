json.array!(@pre_sales) do |pre_sale|
  json.extract! pre_sale, :id, :client_id, :annotation, :return_date, :payment_method, :status
  json.url pre_sale_url(pre_sale, format: :json)
end
