json.array!(@products) do |product|
  json.extract! product, :id, :name, :code, :buy_price, :sell_price, :description, :distributor_id
  json.url product_url(product, format: :json)
end
