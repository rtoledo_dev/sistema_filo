json.array!(@daily_cashes) do |daily_cash|
  json.extract! daily_cash, :id, :sale_id, :receive_type, :amount
  json.url daily_cash_url(daily_cash, format: :json)
end
