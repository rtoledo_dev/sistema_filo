json.array!(@sales) do |sale|
  json.extract! sale, :id, :pre_sale_id, :user_id, :client_id, :annotation, :payment_method, :payment_division, :payment_division_sell_price, :discount, :status
  json.url sale_url(sale, format: :json)
end
