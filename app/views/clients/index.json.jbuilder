json.array!(@clients) do |client|
  json.extract! client, :id, :name, :phone, :mobile, :email, :work, :work_address, :work_phone, :best_day
  json.url client_url(client, format: :json)
end
