class Notifier < ApplicationMailer
  default from: "lojafilolingerie@gmail.com"
  helper :pre_sales, :street_jobs
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.send_pre_sale.subject
  #
  def send_pre_sale(pre_sale)
    @pre_sale = pre_sale
    mail to: User.pluck(:email)
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.receive_pre_sale.subject
  #
  def receive_pre_sale(pre_sale)
    @pre_sale = pre_sale
    mail to: User.pluck(:email)
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.street_job.subject
  #
  def street_job(street_job)
    @street_job = street_job
    mail to: User.pluck(:email)
  end
end
