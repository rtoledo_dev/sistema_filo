class StreetJobsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_street_job, only: [:show, :edit, :update, :destroy]

  # GET /street_jobs
  # GET /street_jobs.json
  def index
    @street_jobs = StreetJob.order(scheduled_at: :desc)
  end

  # GET /street_jobs/1
  # GET /street_jobs/1.json
  def show
  end

  # GET /street_jobs/new
  def new
    @street_job = StreetJob.new
  end

  # GET /street_jobs/1/edit
  def edit
  end

  # POST /street_jobs
  # POST /street_jobs.json
  def create
    @street_job = StreetJob.new(street_job_params)

    respond_to do |format|
      if @street_job.save
        format.html { redirect_to @street_job, notice: 'Operação realizada com sucesso.' }
        format.json { render :show, status: :created, location: @street_job }
      else
        format.html { render :new }
        format.json { render json: @street_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /street_jobs/1
  # PATCH/PUT /street_jobs/1.json
  def update
    respond_to do |format|
      if @street_job.update(street_job_params)
        format.html { redirect_to @street_job, notice: 'Operação realizada com sucesso.' }
        format.json { render :show, status: :ok, location: @street_job }
      else
        format.html { render :edit }
        format.json { render json: @street_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /street_jobs/1
  # DELETE /street_jobs/1.json
  def destroy
    @street_job.destroy
    respond_to do |format|
      format.html { redirect_to street_jobs_url, notice: 'Operação realizada com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_street_job
      @street_job = StreetJob.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def street_job_params
      params.require(:street_job).permit(:client_id, :client_name, :annotation, :receive_street_job_at,
        :send_street_job_at, :payment_method, :status,
        street_job_products_attributes: [:id, :product_name, :detail, :price, :quantity, :_destroy])
    end
end
