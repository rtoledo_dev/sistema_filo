class DailyCashesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_daily_cash, only: [:show, :edit, :update, :destroy]

  # GET /daily_cashes
  # GET /daily_cashes.json
  def index
    @daily_cashes = DailyCash.all
  end

  # GET /daily_cashes/1
  # GET /daily_cashes/1.json
  def show
  end

  # GET /daily_cashes/new
  def new
    @daily_cash = DailyCash.new(sale_id: params[:sale_id])
  end

  # GET /daily_cashes/1/edit
  def edit
  end

  # POST /daily_cashes
  # POST /daily_cashes.json
  def create
    @daily_cash = DailyCash.new(daily_cash_params)

    respond_to do |format|
      if @daily_cash.save
        format.html { redirect_to @daily_cash, notice: 'Operação realizada com sucesso.' }
        format.json { render :show, status: :created, location: @daily_cash }
      else
        format.html { render :new }
        format.json { render json: @daily_cash.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /daily_cashes/1
  # PATCH/PUT /daily_cashes/1.json
  def update
    respond_to do |format|
      if @daily_cash.update(daily_cash_params)
        format.html { redirect_to @daily_cash, notice: 'Operação realizada com sucesso.' }
        format.json { render :show, status: :ok, location: @daily_cash }
      else
        format.html { render :edit }
        format.json { render json: @daily_cash.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /daily_cashes/1
  # DELETE /daily_cashes/1.json
  def destroy
    @daily_cash.destroy
    respond_to do |format|
      format.html { redirect_to daily_cashes_url, notice: 'Operação realizada com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_daily_cash
      @daily_cash = DailyCash.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def daily_cash_params
      params.require(:daily_cash).permit(:sale_id, :receive_type, :amount, :annotation, :created_at)
    end
end
