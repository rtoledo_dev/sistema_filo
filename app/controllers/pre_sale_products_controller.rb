class PreSaleProductsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_pre_sale_product, only: [:show, :edit, :update, :destroy]

  # GET /pre_sale_products
  # GET /pre_sale_products.json
  def index
    @pre_sale_products = PreSaleProduct.all
  end

  # GET /pre_sale_products/1
  # GET /pre_sale_products/1.json
  def show
  end

  # GET /pre_sale_products/new
  def new
    @pre_sale_product = PreSaleProduct.new
  end

  # GET /pre_sale_products/1/edit
  def edit
  end

  # POST /pre_sale_products
  # POST /pre_sale_products.json
  def create
    @pre_sale_product = PreSaleProduct.new(pre_sale_product_params)

    respond_to do |format|
      if @pre_sale_product.save
        format.html { redirect_to @pre_sale_product, notice: 'Pre sale product was successfully created.' }
        format.json { render :show, status: :created, location: @pre_sale_product }
      else
        format.html { render :new }
        format.json { render json: @pre_sale_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pre_sale_products/1
  # PATCH/PUT /pre_sale_products/1.json
  def update
    respond_to do |format|
      if @pre_sale_product.update(pre_sale_product_params)
        format.html { redirect_to @pre_sale_product, notice: 'Pre sale product was successfully updated.' }
        format.json { render :show, status: :ok, location: @pre_sale_product }
      else
        format.html { render :edit }
        format.json { render json: @pre_sale_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pre_sale_products/1
  # DELETE /pre_sale_products/1.json
  def destroy
    @pre_sale_product.destroy
    respond_to do |format|
      format.html { redirect_to pre_sale_products_url, notice: 'Pre sale product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pre_sale_product
      @pre_sale_product = PreSaleProduct.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pre_sale_product_params
      params.require(:pre_sale_product).permit(:pre_safe_id, :product_id, :price, :quantity, :detail)
    end
end
