class DashboardController < ApplicationController
  before_action :authenticate_user!
  def index
    @pre_sales_receive_today = PreSale.receive_today.order(:receive_pre_sale_at)
    @pre_sales_send_today = PreSale.send_today.order(:send_pre_sale_at)
    @street_jobs_today = StreetJob.where("status IS NULL AND scheduled_at <= ?", Date.today.end_of_day).order(:scheduled_at)
    @sales_today = Sale.where(sell_at: Date.today)
    @sales_yesterday = Sale.where(sell_at: Date.yesterday)
    @sales_week = Sale.where(sell_at: Date.today.beginning_of_week..Date.today.end_of_week.end_of_day)
    @daily_cashes = DailyCash.where("created_at >= ?", Date.today.beginning_of_day)
    @daily_cashes_yesterday = DailyCash.where(created_at: Date.yesterday.beginning_of_day..Date.yesterday.end_of_day)
    @daily_cashes_week = DailyCash.where(created_at: Date.today.beginning_of_week.beginning_of_day..Date.today.end_of_week.end_of_day)
  end
end
