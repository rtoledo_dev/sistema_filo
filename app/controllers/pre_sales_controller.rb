class PreSalesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_pre_sale, only: [:show, :edit, :update, :destroy, :update_status]
  autocomplete :client, :name, full: true, extra_data: [:work]
  autocomplete :product, :name, full: true

  # GET /pre_sales
  # GET /pre_sales.json
  def index
    @pre_sales = PreSale.all
  end

  # GET /pre_sales/1
  # GET /pre_sales/1.json
  def show
  end

  # GET /pre_sales/new
  def new
    @pre_sale = PreSale.new(receive_pre_sale_at: Date.today + 3.days)
  end

  # GET /pre_sales/1/edit
  def edit
  end

  def update_status
    @pre_sale.status = params[:status]
    @pre_sale.save
    redirect_by_status(@pre_sale)
  end

  # POST /pre_sales
  # POST /pre_sales.json
  def create
    @pre_sale = PreSale.new(pre_sale_params)

    respond_to do |format|
      if @pre_sale.save_and_schedule
        format.html { redirect_to @pre_sale, notice: 'Operação realizada com sucesso.' }
        format.json { render :show, status: :created, location: @pre_sale }
      else
        format.html { render :new }
        format.json { render json: @pre_sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pre_sales/1
  # PATCH/PUT /pre_sales/1.json
  def update
    respond_to do |format|
      if @pre_sale.update_and_schedule(pre_sale_params)
        format.html { redirect_to @pre_sale, notice: 'Operação realizada com sucesso.' }
        format.json { render :show, status: :ok, location: @pre_sale }
      else
        format.html { render :edit }
        format.json { render json: @pre_sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pre_sales/1
  # DELETE /pre_sales/1.json
  def destroy
    @pre_sale.destroy
    respond_to do |format|
      format.html { redirect_to pre_sales_url, notice: 'Operação realizada com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pre_sale
      @pre_sale = PreSale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pre_sale_params
      params.require(:pre_sale).permit(:client_id, :client_name, :annotation, :receive_pre_sale_at,
        :send_pre_sale_at, :payment_method, :status,
        pre_sale_products_attributes: [:id, :product_name, :detail, :price, :quantity, :_destroy])
    end

    def redirect_by_status(pre_sale)
      if pre_sale.absent_client? || pre_sale.return_other_day?
        redirect_to edit_pre_sale_path(pre_sale), alert: "Reveja as datas de Envio e Recebimento"
      elsif pre_sale.converted_into_sale? && pre_sale.sales.count > 0
        redirect_to edit_sale_path(pre_sale.sales.last), notice: "Conversão em venda feita com sucesso. Termine de completar a venda"
      else
        redirect_to pre_sale_path(pre_sale), notice: "Situação atualizada com sucesso"
      end
    end
end
