module DailyCashesHelper
  def daily_cash_receive_types_options
    daily_cash_receive_types.sort_by{|t,text| text}.to_h.map do |receive_type, text|
      [text, receive_type]
    end
  end

  def daily_cash_receive_types
    receive_types = {}
    DailyCash.receive_types.each do |receive_type, code|
      receive_types[receive_type.to_sym] = I18n.t("activerecord.attributes.daily_cash.receive_types.#{receive_type}")
    end
    receive_types
  end

  def daily_cash_receive_type(receive_type)
    daily_cash_receive_types[receive_type.to_sym]
  end
end
