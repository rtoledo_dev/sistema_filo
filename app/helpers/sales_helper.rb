module SalesHelper
  def sale_statuses_options
    sale_statuses.sort_by{|t,text| text}.to_h.map do |status, text|
      [text, status]
    end
  end

  def sale_statuses
    statuses = {}
    Sale.statuses.each do |status, code|
      statuses[status.to_sym] = I18n.t("activerecord.attributes.sale.statuses.#{status}")
    end
    statuses
  end

  def sale_status(status)
    sale_statuses[status.to_sym]
  rescue
    '-'
  end

  def sale_payment_methods_options
    sale_payment_methods.sort_by{|t,text| text}.to_h.map do |payment_method, text|
      [text, payment_method]
    end
  end

  def sale_payment_methods
    payment_methods = {}
    Sale.payment_methods.each do |payment_method, code|
      payment_methods[payment_method.to_sym] = I18n.t("activerecord.attributes.sale.payment_methods.#{payment_method}")
    end
    payment_methods
  end

  def sale_payment_method(payment_method)
    sale_payment_methods[payment_method.to_sym]
  rescue
    '-'
  end

  def sale_tr_class(sale)
    if sale.finished_and_received?
      'success'
    elsif sale.finished_and_not_received?
      'warning'
    else
      'danger'
    end
  end
end
