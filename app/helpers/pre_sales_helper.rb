module PreSalesHelper
  def pre_sale_statuses_options
    pre_sale_statuses.map do |status, text|
      [text, status]
    end
  end

  def pre_sale_statuses
    statuses = {}
    PreSale.statuses.each do |status, code|
      statuses[status.to_sym] = I18n.t("activerecord.attributes.pre_sale.statuses.#{status}")
    end
    statuses
  end

  def pre_sale_status(status)
    pre_sale_statuses[status.to_sym]
  end
end
