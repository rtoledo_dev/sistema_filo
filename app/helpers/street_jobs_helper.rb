module StreetJobsHelper
  def street_job_statuses_options
    street_job_statuses.map do |status, text|
      [text, status]
    end
  end

  def street_job_statuses
    statuses = {}
    StreetJob.statuses.each do |status, code|
      statuses[status.to_sym] = I18n.t("activerecord.attributes.street_job.statuses.#{status}")
    end
    statuses
  end

  def street_job_status(status)
    status.nil? ? "-" : street_job_statuses[status.to_sym]
  end

  def street_job_operation_types_options
    street_job_operation_types.map do |operation_type, text|
      [text, operation_type]
    end
  end

  def street_job_operation_types
    operation_types = {}
    StreetJob.operation_types.each do |operation_type, code|
      operation_types[operation_type.to_sym] = I18n.t("activerecord.attributes.street_job.operation_types.#{operation_type}")
    end
    operation_types
  end

  def street_job_operation_type(operation_type)
    street_job_operation_types[operation_type.to_sym]
  end
end
