require "rails_helper"

RSpec.describe Notifier, type: :mailer do
  describe "send_pre_sale" do
    let(:mail) { Notifier.send_pre_sale }

    it "renders the headers" do
      expect(mail.subject).to eq("Send pre sale")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "receive_pre_sale" do
    let(:mail) { Notifier.receive_pre_sale }

    it "renders the headers" do
      expect(mail.subject).to eq("Receive pre sale")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
