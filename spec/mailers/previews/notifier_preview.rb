# Preview all emails at http://localhost:3000/rails/mailers/notifier
class NotifierPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/notifier/send_pre_sale
  def send_pre_sale
    Notifier.send_pre_sale
  end

  # Preview this email at http://localhost:3000/rails/mailers/notifier/receive_pre_sale
  def receive_pre_sale
    Notifier.receive_pre_sale
  end

end
