require 'rails_helper'

RSpec.describe "distributors/index", type: :view do
  before(:each) do
    assign(:distributors, [
      Distributor.create!(
        :name => "Name",
        :contact => "Contact",
        :contact_phone => "Contact Phone"
      ),
      Distributor.create!(
        :name => "Name",
        :contact => "Contact",
        :contact_phone => "Contact Phone"
      )
    ])
  end

  it "renders a list of distributors" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Contact".to_s, :count => 2
    assert_select "tr>td", :text => "Contact Phone".to_s, :count => 2
  end
end
