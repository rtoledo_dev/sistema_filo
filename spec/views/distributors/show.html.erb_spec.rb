require 'rails_helper'

RSpec.describe "distributors/show", type: :view do
  before(:each) do
    @distributor = assign(:distributor, Distributor.create!(
      :name => "Name",
      :contact => "Contact",
      :contact_phone => "Contact Phone"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Contact/)
    expect(rendered).to match(/Contact Phone/)
  end
end
