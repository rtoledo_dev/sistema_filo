require 'rails_helper'

RSpec.describe "distributors/new", type: :view do
  before(:each) do
    assign(:distributor, Distributor.new(
      :name => "MyString",
      :contact => "MyString",
      :contact_phone => "MyString"
    ))
  end

  it "renders new distributor form" do
    render

    assert_select "form[action=?][method=?]", distributors_path, "post" do

      assert_select "input#distributor_name[name=?]", "distributor[name]"

      assert_select "input#distributor_contact[name=?]", "distributor[contact]"

      assert_select "input#distributor_contact_phone[name=?]", "distributor[contact_phone]"
    end
  end
end
