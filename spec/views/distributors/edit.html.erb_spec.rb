require 'rails_helper'

RSpec.describe "distributors/edit", type: :view do
  before(:each) do
    @distributor = assign(:distributor, Distributor.create!(
      :name => "MyString",
      :contact => "MyString",
      :contact_phone => "MyString"
    ))
  end

  it "renders the edit distributor form" do
    render

    assert_select "form[action=?][method=?]", distributor_path(@distributor), "post" do

      assert_select "input#distributor_name[name=?]", "distributor[name]"

      assert_select "input#distributor_contact[name=?]", "distributor[contact]"

      assert_select "input#distributor_contact_phone[name=?]", "distributor[contact_phone]"
    end
  end
end
