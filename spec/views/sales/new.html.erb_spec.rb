require 'rails_helper'

RSpec.describe "sales/new", type: :view do
  before(:each) do
    assign(:sale, Sale.new(
      :pre_sale => nil,
      :user => nil,
      :client => nil,
      :annotation => "MyText",
      :payment_method => 1,
      :payment_division => 1,
      :payment_division_sell_price => 1.5,
      :discount => 1.5,
      :status => 1
    ))
  end

  it "renders new sale form" do
    render

    assert_select "form[action=?][method=?]", sales_path, "post" do

      assert_select "input#sale_pre_sale_id[name=?]", "sale[pre_sale_id]"

      assert_select "input#sale_user_id[name=?]", "sale[user_id]"

      assert_select "input#sale_client_id[name=?]", "sale[client_id]"

      assert_select "textarea#sale_annotation[name=?]", "sale[annotation]"

      assert_select "input#sale_payment_method[name=?]", "sale[payment_method]"

      assert_select "input#sale_payment_division[name=?]", "sale[payment_division]"

      assert_select "input#sale_payment_division_sell_price[name=?]", "sale[payment_division_sell_price]"

      assert_select "input#sale_discount[name=?]", "sale[discount]"

      assert_select "input#sale_status[name=?]", "sale[status]"
    end
  end
end
