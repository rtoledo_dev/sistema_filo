require 'rails_helper'

RSpec.describe "sales/index", type: :view do
  before(:each) do
    assign(:sales, [
      Sale.create!(
        :pre_sale => nil,
        :user => nil,
        :client => nil,
        :annotation => "MyText",
        :payment_method => 1,
        :payment_division => 2,
        :payment_division_sell_price => 1.5,
        :discount => 1.5,
        :status => 3
      ),
      Sale.create!(
        :pre_sale => nil,
        :user => nil,
        :client => nil,
        :annotation => "MyText",
        :payment_method => 1,
        :payment_division => 2,
        :payment_division_sell_price => 1.5,
        :discount => 1.5,
        :status => 3
      )
    ])
  end

  it "renders a list of sales" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
