require 'rails_helper'

RSpec.describe "pre_sales/index", type: :view do
  before(:each) do
    assign(:pre_sales, [
      PreSale.create!(
        :client => nil,
        :annotation => "MyText",
        :payment_method => 1,
        :status => 2
      ),
      PreSale.create!(
        :client => nil,
        :annotation => "MyText",
        :payment_method => 1,
        :status => 2
      )
    ])
  end

  it "renders a list of pre_sales" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
