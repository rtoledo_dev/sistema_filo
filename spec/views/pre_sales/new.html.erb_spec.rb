require 'rails_helper'

RSpec.describe "pre_sales/new", type: :view do
  before(:each) do
    assign(:pre_sale, PreSale.new(
      :client => nil,
      :annotation => "MyText",
      :payment_method => 1,
      :status => 1
    ))
  end

  it "renders new pre_sale form" do
    render

    assert_select "form[action=?][method=?]", pre_sales_path, "post" do

      assert_select "input#pre_sale_client_id[name=?]", "pre_sale[client_id]"

      assert_select "textarea#pre_sale_annotation[name=?]", "pre_sale[annotation]"

      assert_select "input#pre_sale_payment_method[name=?]", "pre_sale[payment_method]"

      assert_select "input#pre_sale_status[name=?]", "pre_sale[status]"
    end
  end
end
