require 'rails_helper'

RSpec.describe "pre_sales/show", type: :view do
  before(:each) do
    @pre_sale = assign(:pre_sale, PreSale.create!(
      :client => nil,
      :annotation => "MyText",
      :payment_method => 1,
      :status => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
  end
end
