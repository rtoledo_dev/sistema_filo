require 'rails_helper'

RSpec.describe "products/index", type: :view do
  before(:each) do
    assign(:products, [
      Product.create!(
        :name => "Name",
        :code => "Code",
        :buy_price => 1.5,
        :sell_price => 1.5,
        :description => "MyText",
        :distributor => nil
      ),
      Product.create!(
        :name => "Name",
        :code => "Code",
        :buy_price => 1.5,
        :sell_price => 1.5,
        :description => "MyText",
        :distributor => nil
      )
    ])
  end

  it "renders a list of products" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
