require 'rails_helper'

RSpec.describe "products/new", type: :view do
  before(:each) do
    assign(:product, Product.new(
      :name => "MyString",
      :code => "MyString",
      :buy_price => 1.5,
      :sell_price => 1.5,
      :description => "MyText",
      :distributor => nil
    ))
  end

  it "renders new product form" do
    render

    assert_select "form[action=?][method=?]", products_path, "post" do

      assert_select "input#product_name[name=?]", "product[name]"

      assert_select "input#product_code[name=?]", "product[code]"

      assert_select "input#product_buy_price[name=?]", "product[buy_price]"

      assert_select "input#product_sell_price[name=?]", "product[sell_price]"

      assert_select "textarea#product_description[name=?]", "product[description]"

      assert_select "input#product_distributor_id[name=?]", "product[distributor_id]"
    end
  end
end
