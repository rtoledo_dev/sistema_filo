require 'rails_helper'

RSpec.describe "daily_cashes/new", type: :view do
  before(:each) do
    assign(:daily_cash, DailyCash.new(
      :sale => nil,
      :receive_type => 1,
      :amount => 1.5
    ))
  end

  it "renders new daily_cash form" do
    render

    assert_select "form[action=?][method=?]", daily_cashes_path, "post" do

      assert_select "input#daily_cash_sale_id[name=?]", "daily_cash[sale_id]"

      assert_select "input#daily_cash_receive_type[name=?]", "daily_cash[receive_type]"

      assert_select "input#daily_cash_amount[name=?]", "daily_cash[amount]"
    end
  end
end
