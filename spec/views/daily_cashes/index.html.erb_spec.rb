require 'rails_helper'

RSpec.describe "daily_cashes/index", type: :view do
  before(:each) do
    assign(:daily_cashes, [
      DailyCash.create!(
        :sale => nil,
        :receive_type => 1,
        :amount => 1.5
      ),
      DailyCash.create!(
        :sale => nil,
        :receive_type => 1,
        :amount => 1.5
      )
    ])
  end

  it "renders a list of daily_cashes" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
