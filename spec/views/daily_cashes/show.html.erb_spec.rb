require 'rails_helper'

RSpec.describe "daily_cashes/show", type: :view do
  before(:each) do
    @daily_cash = assign(:daily_cash, DailyCash.create!(
      :sale => nil,
      :receive_type => 1,
      :amount => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/1.5/)
  end
end
