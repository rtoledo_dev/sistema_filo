require 'rails_helper'

RSpec.describe "daily_cashes/edit", type: :view do
  before(:each) do
    @daily_cash = assign(:daily_cash, DailyCash.create!(
      :sale => nil,
      :receive_type => 1,
      :amount => 1.5
    ))
  end

  it "renders the edit daily_cash form" do
    render

    assert_select "form[action=?][method=?]", daily_cash_path(@daily_cash), "post" do

      assert_select "input#daily_cash_sale_id[name=?]", "daily_cash[sale_id]"

      assert_select "input#daily_cash_receive_type[name=?]", "daily_cash[receive_type]"

      assert_select "input#daily_cash_amount[name=?]", "daily_cash[amount]"
    end
  end
end
