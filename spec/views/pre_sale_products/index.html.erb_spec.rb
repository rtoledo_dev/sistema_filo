require 'rails_helper'

RSpec.describe "pre_sale_products/index", type: :view do
  before(:each) do
    assign(:pre_sale_products, [
      PreSaleProduct.create!(
        :pre_safe => nil,
        :product => nil,
        :price => 1.5,
        :quantity => 1,
        :detail => "MyText"
      ),
      PreSaleProduct.create!(
        :pre_safe => nil,
        :product => nil,
        :price => 1.5,
        :quantity => 1,
        :detail => "MyText"
      )
    ])
  end

  it "renders a list of pre_sale_products" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
