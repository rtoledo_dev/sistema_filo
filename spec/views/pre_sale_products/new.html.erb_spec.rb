require 'rails_helper'

RSpec.describe "pre_sale_products/new", type: :view do
  before(:each) do
    assign(:pre_sale_product, PreSaleProduct.new(
      :pre_safe => nil,
      :product => nil,
      :price => 1.5,
      :quantity => 1,
      :detail => "MyText"
    ))
  end

  it "renders new pre_sale_product form" do
    render

    assert_select "form[action=?][method=?]", pre_sale_products_path, "post" do

      assert_select "input#pre_sale_product_pre_safe_id[name=?]", "pre_sale_product[pre_safe_id]"

      assert_select "input#pre_sale_product_product_id[name=?]", "pre_sale_product[product_id]"

      assert_select "input#pre_sale_product_price[name=?]", "pre_sale_product[price]"

      assert_select "input#pre_sale_product_quantity[name=?]", "pre_sale_product[quantity]"

      assert_select "textarea#pre_sale_product_detail[name=?]", "pre_sale_product[detail]"
    end
  end
end
