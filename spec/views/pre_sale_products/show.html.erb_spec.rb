require 'rails_helper'

RSpec.describe "pre_sale_products/show", type: :view do
  before(:each) do
    @pre_sale_product = assign(:pre_sale_product, PreSaleProduct.create!(
      :pre_safe => nil,
      :product => nil,
      :price => 1.5,
      :quantity => 1,
      :detail => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/MyText/)
  end
end
