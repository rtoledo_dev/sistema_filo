require 'rails_helper'

RSpec.describe "clients/show", type: :view do
  before(:each) do
    @client = assign(:client, Client.create!(
      :name => "Name",
      :phone => "Phone",
      :mobile => "Mobile",
      :email => "Email",
      :work => "Work",
      :work_address => "Work Address",
      :work_phone => "Work Phone",
      :best_day => "Best Day"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Phone/)
    expect(rendered).to match(/Mobile/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Work/)
    expect(rendered).to match(/Work Address/)
    expect(rendered).to match(/Work Phone/)
    expect(rendered).to match(/Best Day/)
  end
end
