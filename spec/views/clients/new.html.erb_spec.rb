require 'rails_helper'

RSpec.describe "clients/new", type: :view do
  before(:each) do
    assign(:client, Client.new(
      :name => "MyString",
      :phone => "MyString",
      :mobile => "MyString",
      :email => "MyString",
      :work => "MyString",
      :work_address => "MyString",
      :work_phone => "MyString",
      :best_day => "MyString"
    ))
  end

  it "renders new client form" do
    render

    assert_select "form[action=?][method=?]", clients_path, "post" do

      assert_select "input#client_name[name=?]", "client[name]"

      assert_select "input#client_phone[name=?]", "client[phone]"

      assert_select "input#client_mobile[name=?]", "client[mobile]"

      assert_select "input#client_email[name=?]", "client[email]"

      assert_select "input#client_work[name=?]", "client[work]"

      assert_select "input#client_work_address[name=?]", "client[work_address]"

      assert_select "input#client_work_phone[name=?]", "client[work_phone]"

      assert_select "input#client_best_day[name=?]", "client[best_day]"
    end
  end
end
