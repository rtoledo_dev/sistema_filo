require 'rails_helper'

RSpec.describe "clients/index", type: :view do
  before(:each) do
    assign(:clients, [
      Client.create!(
        :name => "Name",
        :phone => "Phone",
        :mobile => "Mobile",
        :email => "Email",
        :work => "Work",
        :work_address => "Work Address",
        :work_phone => "Work Phone",
        :best_day => "Best Day"
      ),
      Client.create!(
        :name => "Name",
        :phone => "Phone",
        :mobile => "Mobile",
        :email => "Email",
        :work => "Work",
        :work_address => "Work Address",
        :work_phone => "Work Phone",
        :best_day => "Best Day"
      )
    ])
  end

  it "renders a list of clients" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Mobile".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Work".to_s, :count => 2
    assert_select "tr>td", :text => "Work Address".to_s, :count => 2
    assert_select "tr>td", :text => "Work Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Best Day".to_s, :count => 2
  end
end
