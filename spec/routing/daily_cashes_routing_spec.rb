require "rails_helper"

RSpec.describe DailyCashesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/daily_cashes").to route_to("daily_cashes#index")
    end

    it "routes to #new" do
      expect(:get => "/daily_cashes/new").to route_to("daily_cashes#new")
    end

    it "routes to #show" do
      expect(:get => "/daily_cashes/1").to route_to("daily_cashes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/daily_cashes/1/edit").to route_to("daily_cashes#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/daily_cashes").to route_to("daily_cashes#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/daily_cashes/1").to route_to("daily_cashes#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/daily_cashes/1").to route_to("daily_cashes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/daily_cashes/1").to route_to("daily_cashes#destroy", :id => "1")
    end

  end
end
