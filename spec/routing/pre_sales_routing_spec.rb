require "rails_helper"

RSpec.describe PreSalesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/pre_sales").to route_to("pre_sales#index")
    end

    it "routes to #new" do
      expect(:get => "/pre_sales/new").to route_to("pre_sales#new")
    end

    it "routes to #show" do
      expect(:get => "/pre_sales/1").to route_to("pre_sales#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/pre_sales/1/edit").to route_to("pre_sales#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/pre_sales").to route_to("pre_sales#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/pre_sales/1").to route_to("pre_sales#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/pre_sales/1").to route_to("pre_sales#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/pre_sales/1").to route_to("pre_sales#destroy", :id => "1")
    end

  end
end
