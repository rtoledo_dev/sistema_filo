require "rails_helper"

RSpec.describe PreSaleProductsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/pre_sale_products").to route_to("pre_sale_products#index")
    end

    it "routes to #new" do
      expect(:get => "/pre_sale_products/new").to route_to("pre_sale_products#new")
    end

    it "routes to #show" do
      expect(:get => "/pre_sale_products/1").to route_to("pre_sale_products#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/pre_sale_products/1/edit").to route_to("pre_sale_products#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/pre_sale_products").to route_to("pre_sale_products#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/pre_sale_products/1").to route_to("pre_sale_products#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/pre_sale_products/1").to route_to("pre_sale_products#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/pre_sale_products/1").to route_to("pre_sale_products#destroy", :id => "1")
    end

  end
end
