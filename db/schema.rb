# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151113120501) do

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "mobile"
    t.string   "email"
    t.string   "work"
    t.string   "work_address"
    t.string   "work_phone"
    t.string   "best_day"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "daily_cashes", force: :cascade do |t|
    t.integer  "sale_id"
    t.integer  "receive_type"
    t.float    "amount"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.text     "annotation"
  end

  add_index "daily_cashes", ["sale_id"], name: "index_daily_cashes_on_sale_id"

  create_table "distributors", force: :cascade do |t|
    t.string   "name"
    t.string   "contact"
    t.string   "contact_phone"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "pre_sale_products", force: :cascade do |t|
    t.integer  "pre_sale_id"
    t.integer  "product_id"
    t.float    "price"
    t.integer  "quantity"
    t.text     "detail"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "pre_sale_products", ["pre_sale_id"], name: "index_pre_sale_products_on_pre_sale_id"
  add_index "pre_sale_products", ["product_id"], name: "index_pre_sale_products_on_product_id"

  create_table "pre_sales", force: :cascade do |t|
    t.integer  "client_id"
    t.text     "annotation"
    t.date     "receive_pre_sale_at"
    t.integer  "payment_method"
    t.integer  "status"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.datetime "send_pre_sale_at"
  end

  add_index "pre_sales", ["client_id"], name: "index_pre_sales_on_client_id"

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.float    "buy_price"
    t.float    "sell_price"
    t.text     "description"
    t.integer  "distributor_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "products", ["distributor_id"], name: "index_products_on_distributor_id"

  create_table "sale_products", force: :cascade do |t|
    t.integer  "sale_id"
    t.integer  "product_id"
    t.float    "base_price"
    t.float    "price"
    t.integer  "quantity"
    t.text     "detail"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "sale_products", ["product_id"], name: "index_sale_products_on_product_id"

  create_table "sales", force: :cascade do |t|
    t.integer  "pre_sale_id"
    t.integer  "user_id"
    t.integer  "client_id"
    t.text     "annotation"
    t.integer  "payment_method"
    t.integer  "payment_division"
    t.float    "payment_division_sell_price"
    t.float    "discount"
    t.integer  "status"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.date     "sell_at"
    t.integer  "best_day_to_receive"
    t.integer  "sale_id"
    t.float    "money_on_create"
  end

  add_index "sales", ["client_id"], name: "index_sales_on_client_id"
  add_index "sales", ["pre_sale_id"], name: "index_sales_on_pre_sale_id"
  add_index "sales", ["user_id"], name: "index_sales_on_user_id"

  create_table "street_jobs", force: :cascade do |t|
    t.integer  "jobable_id"
    t.string   "jobable_type"
    t.text     "annotation"
    t.integer  "status"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "title"
    t.integer  "operation_type"
    t.datetime "scheduled_at"
  end

  add_index "street_jobs", ["jobable_id"], name: "index_street_jobs_on_jobable_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
