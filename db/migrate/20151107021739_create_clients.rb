class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :phone
      t.string :mobile
      t.string :email
      t.string :work
      t.string :work_address
      t.string :work_phone
      t.string :best_day

      t.timestamps null: false
    end
  end
end
