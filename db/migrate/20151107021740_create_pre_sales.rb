class CreatePreSales < ActiveRecord::Migration
  def change
    create_table :pre_sales do |t|
      t.references :client, index: true, foreign_key: true
      t.text :annotation
      t.date :return_date
      t.integer :payment_method
      t.integer :status

      t.timestamps null: false
    end
  end
end
