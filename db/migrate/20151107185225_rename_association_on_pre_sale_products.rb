class RenameAssociationOnPreSaleProducts < ActiveRecord::Migration
  def change
    rename_column :pre_sale_products, :pre_safe_id, :pre_sale_id
  end
end
