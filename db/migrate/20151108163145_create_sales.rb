class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.references :pre_sale, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.references :client, index: true, foreign_key: true
      t.text :annotation
      t.integer :payment_method
      t.integer :payment_division
      t.float :payment_division_sell_price
      t.float :discount
      t.integer :status

      t.timestamps null: false
    end
  end
end
