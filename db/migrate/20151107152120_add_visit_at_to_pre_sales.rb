class AddVisitAtToPreSales < ActiveRecord::Migration
  def change
    add_column :pre_sales, :visit_at, :datetime
  end
end
