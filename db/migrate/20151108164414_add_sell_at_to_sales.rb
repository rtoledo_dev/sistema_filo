class AddSellAtToSales < ActiveRecord::Migration
  def change
    add_column :sales, :sell_at, :date
  end
end
