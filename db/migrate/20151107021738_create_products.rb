class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :code
      t.float :buy_price
      t.float :sell_price
      t.text :description
      t.references :distributor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
