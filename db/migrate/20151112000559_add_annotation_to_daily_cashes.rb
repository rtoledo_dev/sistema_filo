class AddAnnotationToDailyCashes < ActiveRecord::Migration
  def change
    add_column :daily_cashes, :annotation, :text
  end
end
