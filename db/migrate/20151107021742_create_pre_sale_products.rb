class CreatePreSaleProducts < ActiveRecord::Migration
  def change
    create_table :pre_sale_products do |t|
      t.references :pre_safe, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true
      t.float :price
      t.integer :quantity
      t.text :detail

      t.timestamps null: false
    end
  end
end
