class CreateSaleProducts < ActiveRecord::Migration
  def change
    create_table :sale_products do |t|
      t.references :sale
      t.references :product, index: true, foreign_key: true
      t.float :base_price
      t.float :price
      t.integer :quantity
      t.text :detail

      t.timestamps null: false
    end
  end
end
