class ChangeDatesOnPreSales < ActiveRecord::Migration
  def change
    rename_column :pre_sales, :return_date, :receive_pre_sale_at
    rename_column :pre_sales, :visit_at, :send_pre_sale_at
  end
end
