class AddBestDayToReceiveToSales < ActiveRecord::Migration
  def change
    add_column :sales, :best_day_to_receive, :integer
  end
end
