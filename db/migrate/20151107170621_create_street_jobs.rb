class CreateStreetJobs < ActiveRecord::Migration
  def change
    create_table :street_jobs do |t|
      t.integer :jobable_id
      t.string :jobable_type
      t.text :annotation
      t.integer :status

      t.timestamps null: false
    end
    add_index :street_jobs, :jobable_id
  end
end
