class AddScheduledAtToStreetJobs < ActiveRecord::Migration
  def change
    add_column :street_jobs, :scheduled_at, :datetime
  end
end
