class CreateDailyCashes < ActiveRecord::Migration
  def change
    create_table :daily_cashes do |t|
      t.references :sale, index: true, foreign_key: true
      t.integer :receive_type
      t.float :amount

      t.timestamps null: false
    end
  end
end
