class CreateDistributors < ActiveRecord::Migration
  def change
    create_table :distributors do |t|
      t.string :name
      t.string :contact
      t.string :contact_phone

      t.timestamps null: false
    end
  end
end
