class AddTitleAndOperationTypeToStreetJobs < ActiveRecord::Migration
  def change
    add_column :street_jobs, :title, :string
    add_column :street_jobs, :operation_type, :integer
  end
end
