class AddMoneyOnCreateToSales < ActiveRecord::Migration
  def change
    add_column :sales, :money_on_create, :float
  end
end
